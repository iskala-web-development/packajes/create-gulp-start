// * Import Gulp
import gulp from "gulp"

// * Import Configs
import { path } from "./gulp/configs/index.js"
// * Import Plugins
import plugins from "./gulp/plugins/index.js"
// * Import Tasks
import { reset, html, scss, js, img, otfToTtf, ttfToWoff, vendorJs, vendorCss } from "./gulp/tasks/index.js"

// * Configuration
global.app = {
  isBuild: process.argv.includes("--build"),
  isDev: !process.argv.includes("--build"),
  isWebP: !process.argv.includes("--nowebp"),
  isFontsReW: process.argv.includes("--rewrite"),
  gulp: gulp,
  path: path,
  plugins: plugins
}

const server = () => {
  global.app.plugins.browserSync.init({
    server: {
      baseDir: `${path.buildFolder}`
    },
    notify: false,
    port: 3000
  })
}

const watcher = () => {
  gulp.watch(path.watch.html, html)
  gulp.watch(path.watch.scss, scss)
  gulp.watch(path.watch.js, js)
  gulp.watch(path.watch.img, img)
  gulp.watch(path.watch.vendor.js, vendorJs)
  gulp.watch(path.watch.vendor.css, vendorCss)
  gulp.watch(path.watch.data, html)
}

const defaultTasks = gulp.series(reset, otfToTtf, ttfToWoff, gulp.parallel(html, js, scss, img, vendorJs, vendorCss));

const development = gulp.series(defaultTasks, gulp.parallel(watcher, server));
const build = gulp.series(defaultTasks);
// const deployFTP = gulp.series(buildTasks, ftp);
// const deployZIP = gulp.series(buildTasks, zip);

// * Export Scenarios
export { development }
export { build }
// export { deployFTP }
// export { deployZIP }


// Inti task default gulp
gulp.task('default', development);