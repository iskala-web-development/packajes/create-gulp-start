# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.1.14](https://gitlab.com/iskala-web-development/packajes/create-gulp-start/compare/v1.1.13...v1.1.14) (2024-03-10)

### [1.1.13](https://gitlab.com/iskala-web-development/packajes/create-gulp-start/compare/v1.1.12...v1.1.13) (2024-03-10)

### [1.1.12](https://gitlab.com/iskala-web-development/packajes/create-gulp-start/compare/v1.1.11...v1.1.12) (2024-03-10)

### [1.1.11](https://gitlab.com/iskala-web-development/packajes/create-gulp-start/compare/v1.1.9...v1.1.11) (2024-03-10)

### [1.1.10](https://gitlab.com/iskala-web-development/packajes/create-gulp-start/compare/v1.1.9...v1.1.10) (2024-03-10)

### [1.1.9](https://gitlab.com/iskala-web-development/packajes/create-gulp-start/compare/v1.1.8...v1.1.9) (2024-03-10)

### [1.1.8](https://gitlab.com/iskala-web-development/packajes/create-gulp-start/compare/v1.1.7...v1.1.8) (2024-03-10)

### [1.1.7](https://gitlab.com/iskala-web-development/packajes/create-gulp-start/compare/v1.1.6...v1.1.7) (2024-03-10)

### [1.1.6](https://gitlab.com/iskala-web-development/packajes/create-gulp-start/compare/v1.1.5...v1.1.6) (2024-03-10)

### [1.1.5](https://gitlab.com/iskala-web-development/packajes/create-gulp-start/compare/v1.1.4...v1.1.5) (2024-03-10)

### [1.1.4](https://gitlab.com/iskala-web-development/packajes/create-gulp-start/compare/v1.1.10...v1.1.4) (2024-03-10)

### 1.1.10 (2024-03-10)

### [1.1.9](https://gitlab.com/iskala-web-development/create-gulp-start/compare/v1.1.8...v1.1.9) (2024-03-10)

### [1.1.8](https://gitlab.com/iskala-web-development/create-gulp-start/compare/v1.1.7...v1.1.8) (2024-03-10)

### [1.1.7](https://gitlab.com/iskala-web-development/create-gulp-start/compare/v1.1.6...v1.1.7) (2024-03-10)

### [1.1.6](https://gitlab.com/iskala-web-development/create-gulp-start/compare/v1.1.5...v1.1.6) (2024-03-10)

### [1.1.5](https://gitlab.com/iskala-web-development/create-gulp-start/compare/v1.1.4...v1.1.5) (2024-03-10)

### [1.1.4](https://gitlab.com/iskala-web-development/create-gulp-start/compare/v1.1.3...v1.1.4) (2024-03-10)

### [1.1.3](https://gitlab.com/iskala-web-development/create-gulp-start/compare/v1.1.2...v1.1.3) (2024-03-10)

### [1.1.2](https://gitlab.com/iskala-web-development/create-gulp-start/compare/v1.1.1...v1.1.2) (2024-03-10)

### 1.1.1 (2024-03-10)
