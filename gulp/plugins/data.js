import fs from 'fs'
import data from 'gulp-data';

export default () => data(function(file) {
    const index = file.relative.replace('.html', '');
    const configPath = `${app.path.src.data}/config.json`
    const pageConfigPath = `${app.path.src.data}/pages/${index}.json`

    if (!fs.existsSync(app.path.src.data))
        fs.mkdirSync(app.path.src.data);

    if (!fs.existsSync(configPath))
        fs.writeFileSync(configPath, JSON.stringify({}), cb)

    if (!fs.existsSync(`${app.path.src.data}/pages`))
        fs.mkdirSync(`${app.path.src.data}/pages`);

    if (!fs.existsSync(pageConfigPath))
        fs.writeFileSync(pageConfigPath, JSON.stringify({}), cb)
    
    const config = Object.assign({}, JSON.parse(fs.readFileSync(configPath)), JSON.parse(fs.readFileSync(pageConfigPath)));

    return config
})

const cb = () => {}