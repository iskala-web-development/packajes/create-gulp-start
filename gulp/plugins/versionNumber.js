import versionNumber from 'gulp-version-number'

export default () => versionNumber({
    'value': '%DT%',
    'append': {
        'key': '_v',
        'cover': 0,
        'to': ['css', 'js', 'img']
    },
    'output': {
        'file': './version.json'
    }
})