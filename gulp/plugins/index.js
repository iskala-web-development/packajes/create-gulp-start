// * Import Modules

import plumber from "gulp-plumber";
import versionNumber from './versionNumber.js'
import notify from "gulp-notify";
import newer from "gulp-newer";
import prettier from "gulp-prettier";
import ifPlugin from "gulp-if";
import rename from 'gulp-rename';
import replace from 'gulp-replace';
import concat from 'gulp-concat';
import sourcemaps from 'gulp-sourcemaps';
import minify from 'gulp-minify';
import data from './data.js';
import browserSync from 'browser-sync';
import uglify from 'gulp-uglify';
import flatten from 'gulp-flatten';

// BEGIN: HTML
import nunjucksRender from 'gulp-nunjucks-render'
import webpHtml from 'gulp-webp-html'
import formatHtml from 'gulp-format-html'
import webpHtmlNosvg from 'gulp-webp-html-nosvg'
// END: HTML

// BEGIN: SCSS
import dartSass from 'sass'
import gulpSass from 'gulp-sass'
import cleanCss from 'gulp-clean-css'
import webpCss from 'gulp-webpcss'
import autoprefixer from 'gulp-autoprefixer'
import groupCssMediaQueries from 'gulp-group-css-media-queries'
const sass = gulpSass(dartSass)
// END: SCSS

// BEGIN: JS
import webpack from "webpack";
import webpackStream from "webpack-stream";
// END: JS

// BEGIN: IMG
import imagemin from "gulp-imagemin";
import webp from "gulp-webp";
// END: IMG

// BEGIN: FONTS
import fonter from 'gulp-fonter-fix';
import ttf2woff2 from 'gulp-ttf2woff2';
// END: FONTS

// Экспортируем объект
export default {
	plumber, versionNumber, notify, newer, prettier, if: ifPlugin, rename, replace, concat, sourcemaps, minify, data, browserSync, uglify, flatten,

	// BEGIN: HTML
	nunjucksRender, webpHtml, formatHtml, webpHtmlNosvg,
	// END: HTML

	// BEGIN: SCSS
	dartSass, gulpSass, sass, cleanCss, webpCss, autoprefixer, groupCssMediaQueries,
	// END: SCSS

	// BEGIN: JS
	webpack, webpackStream,
	// END: JS

	// BEGIN: IMG
	imagemin, webp,
	// END: IMG

	// BEGIN: FONTS
	fonter, ttf2woff2
	// END: FONTS
}