import * as nodePath from "path";
const roodFolder = nodePath.basename(nodePath.resolve());

const buildFolder = nodePath.resolve("dist");
const srcFolder = nodePath.resolve("src");

export default {
  build: {
    html: `${buildFolder}/`,
    css: `${buildFolder}/css/`,
    js: `${buildFolder}/js/`,
    img: `${buildFolder}/img/`,
    fonts: `${buildFolder}/fonts/`,
    vendor: `${buildFolder}/vendor/`,
  },
  src: {
    html: {
      layouts: `${srcFolder}/layouts/`,
      components: `${srcFolder}/components/`,
      pages: `${srcFolder}/pages/**/*.html`,
      app: `${srcFolder}/app.html`
    },
    scss: `${srcFolder}/assets/scss/*.scss`,
    js: `${srcFolder}/assets/js/*.js`,
    img: `${srcFolder}/assets/img/**/*.{jpg,jpeg,png,gif,webp}`,
    fonts: `${srcFolder}/assets/fonts/`,
    vendor: {
      js: `${srcFolder}/assets/vendor/**/*.js`,
      css: `${srcFolder}/assets/vendor/**/*.css`
    },
    data: `${srcFolder}/data`
  },
  watch: {
    html: `${srcFolder}/**/*.html`,
    scss: `${srcFolder}/**/*.scss`,
    js: `${srcFolder}/**/*.js`,
    img: `${srcFolder}/**/*.{jpg,jpeg,png,gif,webp}`,
    fonts: `${srcFolder}/**/*.{ttf,woff2,woff,otf}`,
    vendor: {
      js: `${srcFolder}/assets/vendor/**/*.js`,
      css: `${srcFolder}/assets/vendor/**/*.css`
    },
    data: `${srcFolder}/data`
  },
  entryJs: `${srcFolder}/assets/js/app.js`,
  buildFolder: buildFolder,
  srcFolder: srcFolder,
  srcAssetsFolder: `${srcFolder}/assets`,
  roodFolder: roodFolder,
  ftp: ``,
};
