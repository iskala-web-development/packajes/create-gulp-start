export const js = () => 
	app.gulp.src(app.path.src.vendor.js)
    .pipe(app.plugins.flatten({ includeParents: 1}))
    .pipe(app.gulp.dest(app.path.build.vendor))
    .pipe(app.plugins.browserSync.stream())

export const css = () => 
    app.gulp.src(app.path.src.vendor.css)
    .pipe(app.plugins.flatten({ includeParents: 1}))
    .pipe(app.gulp.dest(app.path.build.vendor))
    .pipe(app.plugins.browserSync.stream())
