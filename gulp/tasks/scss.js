export default () => 
	app.gulp.src(app.path.src.scss, { sourcemap: true })
		// .pipe(app.plugins.plumber(
		// 	app.plugins.notify.onError({
		// 		title: "HTML",
		// 		message: "Error: <%= error.message %>"
		// 	}))
		// )
    .pipe(app.plugins.sourcemaps.init())
    // .pipe(app.plugins.webpCss({
    //   webpClass: ".webp",
    //   noWebpClass: ".no-webp",
    // }))
		.pipe(app.plugins.sass({
      outputStyle: 'compressed'
    }))
    .pipe(app.plugins.groupCssMediaQueries())

    .pipe(app.plugins.autoprefixer({
      grid: true,
      overrideBrowserslist: ["last 3 versions"],
      cascade: true
    }))
    .pipe(app.plugins.cleanCss())
    .pipe(app.plugins.replace(/@assets\//g, ''))
    .pipe(app.plugins.concat('app.min.css'))
    .pipe(app.plugins.sourcemaps.write())
		.pipe(app.gulp.dest(app.path.build.css))
    .pipe(app.plugins.browserSync.stream())