export default () => 
	app.gulp.src(app.path.src.img, { sourcemap: true })
    .pipe(app.plugins.newer(app.path.build.img))
    .pipe(
        app.plugins.if(
            app.isWebP,
            app.plugins.webp()
        )
    )
    .pipe(
        app.plugins.if(
            app.isWebP,
            app.gulp.dest(app.path.build.img)
        )
    )
    .pipe(
        app.plugins.if(
            app.isWebP,
            app.gulp.src(app.path.src.img)
        )
    )
    .pipe(
        app.plugins.if(
            app.isWebP,
            app.plugins.newer(app.path.build.img)
        )
    )
    .pipe(app.plugins.imagemin({
        progressive: true,
        svgoPlugins: [{ removeViewBox: false }],
        interlaced: true,
        optimizationLevel: 3 // 0 to 7
    }))
	.pipe(app.gulp.dest(app.path.build.img))
    .pipe(app.plugins.browserSync.stream())