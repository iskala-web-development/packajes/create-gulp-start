export default () => 
	app.gulp.src(app.path.src.html.pages)
		.pipe(app.plugins.data(function(file) {
			console.log(file)
			// return JSON.parse(fs.readFileSync('./examples/' + path.basename(file.path) + '.json'));
		}))
		.pipe(
			app.plugins.if(
				app.isWebP,
				app.plugins.webpHtmlNosvg()
			)
		)
		.pipe(app.plugins.versionNumber())
		.pipe(app.plugins.nunjucksRender({ 
			path: [
				app.path.src.html.app,
				app.path.src.html.layouts,
				app.path.src.html.components
			]
		}))
		.pipe(app.plugins.webpHtml())
		.pipe(app.plugins.formatHtml())
		.pipe(app.plugins.replace(/@assets\//g, ''))
		.pipe(app.gulp.dest(app.path.build.html))
		.pipe(app.plugins.browserSync.stream())