import webpackConfig from '../../webpack.config.js'

export default () => 
	app.gulp.src(app.path.src.js, { sourcemap: true })
    .pipe(app.plugins.webpackStream({config: webpackConfig}))
		.pipe(app.gulp.dest(app.path.build.js))
    .pipe(app.plugins.browserSync.stream())