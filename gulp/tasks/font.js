export const otfToTtf = () => {
	// Ищем файлы шрифтов .otf
	return app.gulp.src(`${app.path.src.fonts}/*.otf`, {})
		// Конвертируем в .ttf
		.pipe(app.plugins.fonter({
			formats: ['ttf']
		}))
		// Выгружаем в исходную папку
		.pipe(app.gulp.dest(app.path.build.fonts))
}

export const ttfToWoff = () => {
	// Ищем файлы шрифтов .ttf
	return app.gulp.src(`${app.path.src.fonts}/*.ttf`, {})
		// Конвертируем в .woff
		.pipe(app.plugins.fonter({
			formats: ['woff']
		}))
		// Выгружаем в папку с результатом
		.pipe(app.gulp.dest(app.path.build.fonts))
		// Ищем файлы шрифтов .ttf
		.pipe(app.gulp.src(`${app.path.src.fonts}/*.ttf`))
		// Конвертируем в .woff2
		.pipe(app.plugins.ttf2woff2())
		// Выгружаем в папку с результатом
		.pipe(app.gulp.dest(`${app.path.build.fonts}`))
		// Ищем файлы шрифтов .woff и woff2
		.pipe(app.gulp.src(`${app.path.src.fonts}/*.{woff,woff2}`))
		// Выгружаем в папку с результатом
		.pipe(app.gulp.dest(`${app.path.build.fonts}`));
}