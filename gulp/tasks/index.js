import reset from './reset.js'
import html from './html.js'
import scss from './scss.js'
import js from './js.js'
import img from './img.js'
import { otfToTtf, ttfToWoff } from './font.js'
import { js as vendorJs, css as vendorCss } from './vendor.js'

export {
    reset, html, scss, js, img, otfToTtf, ttfToWoff, vendorJs, vendorCss, 
}