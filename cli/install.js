#!/usr/bin/env node
import { promisify } from "util";
import cp from "child_process";
import path from "path";
import fs, { existsSync, mkdirSync } from "fs";
import ora from "ora";

const exec = promisify(cp.exec);
const rm = promisify(fs.rm);

if (process.argv.length < 3) {
    console.log("Вы не верно ввели команду.");
    console.log("Пример: npx create-nuxt-start my-app");
    process.exit(1);
}

const projectName = process.argv[2];
const currentPath = process.cwd();
const projectPath = path.join(currentPath, projectName);
const gitRepo = "https://gitlab.com/iskala-web-development/packajes/create-gulp-start.git";

// create project directory
if (fs.existsSync(projectPath)) {
    console.log(`Директория: ${projectName} уже существует.`);
    process.exit(1);
}
else {
    fs.mkdirSync(projectPath);
}

const gitSpinner = ora("Скачивание файлов...")

try {
    gitSpinner.start();
    await exec(`git clone --depth 1 ${gitRepo} ${projectPath} --quiet`);
    gitSpinner.succeed();

    const statusSpinner = ora("Выполняю магию...").start();

    rm(path.join(projectPath, ".git"), { recursive: true, force: true });
    rm(path.join(projectPath, "cli"), { recursive: true, force: true });
    rm(path.join(projectPath, ".gitlab-ci.yml"), { recursive: false, force: true });
    rm(path.join(projectPath, ".npmignore"), { recursive: false, force: true });

    process.chdir(projectPath);

    // remove the packages needed for cli
    await exec("bun remove ora cli-spinners");
    statusSpinner.succeed();

    const npmSpinner = ora("Установка зависимостей...").start();
    await exec("bun install");
    npmSpinner.succeed();

    console.log("Установка завершена!");
    console.log("Выполните следующие шаги:");
    console.log(`cd ${projectName}`);
    console.log(`bun run dev`);
}catch (error) {
    // clean up in case of error, so the user does not have to do it manually
    fs.rmSync(projectPath, { recursive: true, force: true });
    gitSpinner.warn('Ошибка');
    console.log(error);
    process.exit(1);
}