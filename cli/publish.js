#!/usr/bin/env node

// ! DEPRECATED

import { promisify } from "util";
import cp from "child_process";
import path from "path";
import fs, { existsSync, mkdirSync } from "fs";
import ora from "ora";

const exec = promisify(cp.exec);
const rm = promisify(fs.rm);

// if (process.argv.length < 3) {
//     console.log("Вы не верно ввели команду.");
//     console.log("Пример: npx create-nuxt-start my-app");
//     process.exit(1);
// }

const checkSpinner = ora("Проверка конфигураций..").start();

const execBranch = await exec(`git rev-parse --abbrev-ref HEAD`)

if(!execBranch.stdout.trim()){
    console.error("git не инициализирован.");
    process.exit(1);
}

if(!process.argv[2]){
    console.error("Вы не верно ввели команду.");
    console.log("Пример: node cli/publish.js ${commit}");
    process.exit(1);
}

const commit = process.argv[2].trim();
const currentBranch = execBranch.stdout.trim()
const skipNpm = process.argv[3] && process.argv[3].trim() === '--skip-npm' ? true : false

checkSpinner.succeed()

if(!skipNpm){
    const npmSpinner = ora("Публикация в npm..");

    try {
        npmSpinner.start();
        await exec("bun release");
        await exec("npm publish --access public");
        npmSpinner.succeed()
    }catch(error){
        npmSpinner.warn('Ошибка публикации в npm')
        console.error(error);
        console.error('Проверьте версию');
        process.exit(1);
    }
}

const gitSpinner = ora("Публикация в git..");

try {
    gitSpinner.start();
    await exec("git add .");
    await exec(`git commit -m '${commit}'`);
    await exec(`git push origin ${currentBranch}`);
    gitSpinner.succeed()
}catch(error){
    gitSpinner.warn('Ошибка публикации в npm')
    console.error(error);
    process.exit(1);
}
