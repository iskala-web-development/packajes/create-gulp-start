import path from './gulp/configs/path.js'

export default {
    mode: process.env.NODE_ENV ?? 'development',
    entry: path.entryJs, // Ваш основной файл приложения
    output: {
		path: `${path.buildFolder}`,
		filename: 'app.min.js',
		publicPath: '/'
	},
    resolve: {
      extensions: ['.ts', '.js'],
      alias: {
        "@assets": `${path.srcAssetsFolder}`
    },
    },
    module: {
      rules: [
        {
          test: /\.ts$/,
          use: 'babel-loader',
          exclude: /node_modules/,
        },
      ],
    },
  };